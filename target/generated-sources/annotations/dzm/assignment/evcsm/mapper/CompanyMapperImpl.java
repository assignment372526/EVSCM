package dzm.assignment.evcsm.mapper;

import dzm.assignment.evcsm.dto.CompanyDTO;
import dzm.assignment.evcsm.model.Company;
import dzm.assignment.evcsm.model.LocationDto;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-01T17:29:38+0100",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
public class CompanyMapperImpl implements CompanyMapper {

    @Override
    public CompanyDTO dtoToEntityMapping(Company company) {
        if ( company == null ) {
            return null;
        }

        CompanyDTO companyDTO = new CompanyDTO();

        companyDTO.setId( company.getId() );
        companyDTO.setParentCompany( company.getParentCompany() );
        companyDTO.setName( company.getName() );
        companyDTO.setParentCompanyId( company.getParentCompanyId() );

        return companyDTO;
    }

    @Override
    public Company entityToDtoMapping(CompanyDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Company company = new Company();

        company.setId( dto.getId() );
        company.setParentCompany( dto.getParentCompany() );
        company.setName( dto.getName() );
        company.setParentCompanyId( dto.getParentCompanyId() );

        return company;
    }

    @Override
    public Company locationDtoToCompany(LocationDto locationDto) {
        if ( locationDto == null ) {
            return null;
        }

        Company company = new Company();

        company.setParentCompany( locationDtoToCompany1( locationDto ) );

        return company;
    }

    protected Company locationDtoToCompany1(LocationDto locationDto) {
        if ( locationDto == null ) {
            return null;
        }

        Company company = new Company();

        if ( locationDto.getCompanyId() != null ) {
            company.setId( locationDto.getCompanyId() );
        }

        return company;
    }
}
