package dzm.assignment.evcsm.mapper;

import dzm.assignment.evcsm.dto.StationDTO;
import dzm.assignment.evcsm.model.Station;
import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-07-01T17:29:38+0100",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 20 (Oracle Corporation)"
)
public class StationMapperImpl implements StationMapper {

    @Override
    public StationDTO dtoToEntityMapping(Station station) {
        if ( station == null ) {
            return null;
        }

        StationDTO stationDTO = new StationDTO();

        stationDTO.setId( station.getId() );
        stationDTO.setName( station.getName() );
        stationDTO.setLongitude( station.getLongitude() );
        stationDTO.setLatitude( station.getLatitude() );

        return stationDTO;
    }

    @Override
    public Station entityToDtoMapping(StationDTO dto) {
        if ( dto == null ) {
            return null;
        }

        Station.StationBuilder station = Station.builder();

        station.id( dto.getId() );
        station.name( dto.getName() );
        station.longitude( dto.getLongitude() );
        station.latitude( dto.getLatitude() );

        return station.build();
    }
}
