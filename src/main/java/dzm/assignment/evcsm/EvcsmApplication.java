package dzm.assignment.evcsm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EvcsmApplication {

    public static void main(String[] args) {
        SpringApplication.run(EvcsmApplication.class, args);
    }

}
