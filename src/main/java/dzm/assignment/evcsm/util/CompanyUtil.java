package dzm.assignment.evcsm.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dzm.assignment.evcsm.model.Company;

import java.util.ArrayList;
import java.util.List;

public class CompanyUtil {
    List<Company> allChildren = new ArrayList<>();
    public  List<Company> fetchAllChild(Company parent) {
        allChildren.add(parent);
        return getAllChildren(parent);
    }

    //recursive function to walk the tree
    private  List<Company> getAllChildren(Company parent) {

        for (Company child : parent.getChilds()) {
            allChildren.add(child);
            allChildren.addAll(getAllChildren(child));
        }

        return allChildren;
    }
}
