package dzm.assignment.evcsm.config;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Configuration;

@Configuration
@OpenAPIDefinition
public class SwaggerConfig {
    public OpenAPI api() {
        return new OpenAPI()
                .info(new Info().title("Charging Station Management")
                        .version("1.0").description("Charging Station Management API"));
    }
}
