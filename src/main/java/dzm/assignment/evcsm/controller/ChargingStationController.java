package dzm.assignment.evcsm.controller;

import dzm.assignment.evcsm.dto.StationDTO;
import dzm.assignment.evcsm.model.LocationDto;
import dzm.assignment.evcsm.service.ChargingStationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class ChargingStationController {

   @Autowired
   private ChargingStationService service;


    @PostMapping("/stations")
    public StationDTO addStation(@Valid  @RequestBody StationDTO station) {
        return service.addStation(station);
    }

    @PutMapping("/station")
    public StationDTO editStation(@Valid @RequestBody StationDTO station){
        return service.addStation(station);
    }

    @PostMapping(value = "/nearest/stations")
    public List<StationDTO> getStationNearByCar(@Valid @RequestBody LocationDto locationDto){
        return service.getStationNearByCarDistance(locationDto);
    }
}
