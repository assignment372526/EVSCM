package dzm.assignment.evcsm.model;

import lombok.*;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class Location {
    private double longitude;
    private double latitude;
}
