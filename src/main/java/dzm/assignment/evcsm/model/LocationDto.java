package dzm.assignment.evcsm.model;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class LocationDto {
    private Integer companyId;
    private Location carLocation;
    private double distance;
}
