package dzm.assignment.evcsm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
@Builder
@Entity
public class Station {

    @Id
    @GeneratedValue
    private int id;
    @NotNull(message = "Station should not be null")
    private String name;
    @NotNull(message = "longitude should not be null")
    private double longitude;
    @NotNull(message = "latitude should not be null")
    private double latitude;
    @ManyToOne
    @JoinColumn(name = "company_id", nullable = true)
    private Company company;
    @Transient
    @JsonIgnore
    private double stationLocation;
}
