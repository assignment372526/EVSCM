package dzm.assignment.evcsm.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Company {

    @Id
    @GeneratedValue
    private int id;
    @ManyToOne(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENTID", referencedColumnName = "id")
    @JsonIgnore
    private Company parentCompany;
    //    private Integer parentCompany;
    private String name;

    @OneToMany(mappedBy = "parentCompany")
    @JsonIgnore
    private Set<Company> childs = new HashSet<Company>();

    @Transient
    private Integer parentCompanyId;


}

