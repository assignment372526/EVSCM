package dzm.assignment.evcsm.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import dzm.assignment.evcsm.model.Company;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CompanyDTO {
    private int id;
    @JsonIgnore
    private Company parentCompany;
    private String name;
    private Integer parentCompanyId;

}
