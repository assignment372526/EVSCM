package dzm.assignment.evcsm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class StationDTO {
    private int id;
    private String name;
    private double longitude;
    private double latitude;
    private CompanyDTO companyDTO;


}
