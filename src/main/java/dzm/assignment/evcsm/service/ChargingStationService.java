package dzm.assignment.evcsm.service;

import dzm.assignment.evcsm.dto.StationDTO;
import dzm.assignment.evcsm.mapper.CompanyMapper;
import dzm.assignment.evcsm.mapper.StationMapper;
import dzm.assignment.evcsm.model.Company;
import dzm.assignment.evcsm.model.LocationDto;
import dzm.assignment.evcsm.model.Station;
import dzm.assignment.evcsm.repository.CompanyRepository;
import dzm.assignment.evcsm.repository.StationRepository;
import dzm.assignment.evcsm.util.CompanyUtil;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ChargingStationService {

    private final StationRepository stationRepository;

    private final CompanyRepository companyRepository;

    @Autowired
    public ChargingStationService(StationRepository stationRepository, CompanyRepository companyRepository) {
        this.stationRepository = stationRepository;
        this.companyRepository = companyRepository;
    }

    @Transactional
    public StationDTO addStation(StationDTO stationDto) {
        if (!Objects.isNull(stationDto.getCompanyDTO().getParentCompanyId())) {
            stationDto.getCompanyDTO().setParentCompany(
                    companyRepository.findById(stationDto.getCompanyDTO().getParentCompanyId()).orElse(null));
        }
        Company savedCom = companyRepository.save(CompanyMapper.COMPANY_MAPPER.entityToDtoMapping(stationDto.getCompanyDTO()));
        stationDto.setCompanyDTO(CompanyMapper.COMPANY_MAPPER.dtoToEntityMapping(savedCom));
        Station savedStation = stationRepository.save(StationMapper.STATION_MAPPER.entityToDtoMapping(stationDto));
        stationDto.setId(savedStation.getId());
        return stationDto;
    }

    public List<StationDTO> getStationNearByCarDistance(LocationDto locationDto) {
        Company currenntCompany = companyRepository.findById(locationDto.getCompanyId()).orElseThrow(IllegalArgumentException::new);
        List<Company> childCompanies = new CompanyUtil().fetchAllChild(currenntCompany);
        List<Station> allStation = stationRepository.findAllById(
                childCompanies.stream().map(Company::getId).collect(Collectors.toList()));
        List<StationDTO> sortedStation = allStation.stream().peek(station ->
                        station.setStationLocation(Math.sqrt(squre(station.getLatitude() - locationDto.getCarLocation().getLatitude()) +
                                squre(station.getLongitude() - locationDto.getCarLocation().getLongitude())))
                )
                .filter(dist -> dist.getStationLocation() < locationDto.getDistance())
                .sorted(Comparator.comparing(Station::getStationLocation))
                .map(StationMapper.STATION_MAPPER::entityToDtoMapping)
                .collect(Collectors.toList());
        return sortedStation;
    }

    private double squre(double axis) {
        return Math.pow(axis, 2);
    }
}
