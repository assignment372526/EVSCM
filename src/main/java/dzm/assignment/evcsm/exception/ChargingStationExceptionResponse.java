package dzm.assignment.evcsm.exception;


import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@ToString
public class ChargingStationExceptionResponse {

    private String errMsg;
    private String businessCode;
    private String uriInfo;

}
