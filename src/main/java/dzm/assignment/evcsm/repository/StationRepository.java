package dzm.assignment.evcsm.repository;

import dzm.assignment.evcsm.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StationRepository extends JpaRepository<Station,Integer > {

}
