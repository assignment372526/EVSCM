package dzm.assignment.evcsm.repository;

import dzm.assignment.evcsm.model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Integer> {

}
