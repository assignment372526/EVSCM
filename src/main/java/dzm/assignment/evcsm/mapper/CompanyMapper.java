package dzm.assignment.evcsm.mapper;

import dzm.assignment.evcsm.dto.CompanyDTO;
import dzm.assignment.evcsm.model.Company;
import dzm.assignment.evcsm.model.LocationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CompanyMapper {
    CompanyDTO dtoToEntityMapping(Company company);

    Company entityToDtoMapping(CompanyDTO dto);

    @Mappings({
            @Mapping(source = "companyId", target = "parentCompany.id")
    })
    Company locationDtoToCompany(LocationDto locationDto);

    CompanyMapper COMPANY_MAPPER = Mappers.getMapper(CompanyMapper.class);

}
