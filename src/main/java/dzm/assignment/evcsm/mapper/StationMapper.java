package dzm.assignment.evcsm.mapper;

import dzm.assignment.evcsm.dto.StationDTO;
import dzm.assignment.evcsm.model.Station;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StationMapper {
    StationDTO dtoToEntityMapping(Station station);

    Station entityToDtoMapping(StationDTO dto);

    StationMapper STATION_MAPPER = Mappers.getMapper(StationMapper.class);

    default StationDTO entityToDtoMapping(Station station) {
        StationDTO stationDTO = new StationDTO();
        stationDTO.setId(station.getId());
        stationDTO.setName(station.getName());
        stationDTO.setLongitude(stationDTO.getLongitude());
        stationDTO.setLatitude(station.getLatitude());
        return stationDTO;
    }

}
